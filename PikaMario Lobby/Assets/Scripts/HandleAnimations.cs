﻿using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class HandleAnimations : NetworkBehaviour {
    [SerializeField]
    private NetworkAnimator networkAnimator = null;
    Animator anim;

    private PlayerMovement playerMovement;
    private Rigidbody rb;

    [SyncVar]
    StateManager states;
    Vector3 lookDirection;

    //public override void OnStartLocalPlayer () {
     void Start() {
        Debug.Log ($"OnStartClient HandleAnimations");
        states = GetComponent<StateManager> ();
        playerMovement = GetComponent<PlayerMovement>();
        rb = GetComponent<Rigidbody> ();
        SetupAnimator ();

        StartCoroutine (UpdateClient ());
        StartCoroutine (UpdateClient2 ());
    }

    public override void OnStartAuthority () {
        // StartCoroutine(UpdateClient());
        //StartCoroutine(UpdateClient2());

    }

    [Client]
    IEnumerator UpdateClient () //DUNNO
    {
        while (true) {
            if (netIdentity.hasAuthority) {
                //UpdateClient2();

                //if (!hasAuthority) { return; }
                if (!hasAuthority) {
                    yield return new WaitForFixedUpdate ();
                    continue;
                }

                if (states == null) {
                    states = GetComponent<StateManager> ();
                }
                states.reloading = anim.GetBool ("Reloading");
                anim.SetBool ("Aim", states.aiming);
                //anim.SetBool("Aim", false);

                float horizontal = Input.GetAxis ("Horizontal");
                float vertical = Input.GetAxis("Vertical");

                //anim.SetFloat("Forward", vertical, 0.1f, Time.deltaTime);
                // anim.SetFloat("Sideways", horizontal, 0.1f, Time.deltaTime);

                //  CmdMoveAnimate();

                if (!states.canRun) {
                    anim.SetFloat ("Forward", vertical, 0.1f, Time.deltaTime);
                    anim.SetFloat ("Sideways", horizontal, 0.1f, Time.deltaTime);

                } else {
                    float movement = Mathf.Abs (vertical) + Mathf.Abs (states.horizontal);

                    bool walk = states.walk;

                    //movement = Mathf.Clamp (movement, 0, (walk || states.reloading) ? 0.5f : 1); //regular
                    movement = Mathf.Clamp(movement, 0, (walk || states.reloading) ? 0.333f : 0.666f);

                    anim.SetFloat ("Forward", movement, 0.1f, Time.deltaTime);

                    if (states.sprint && movement > 0.1f && playerMovement.stamina > 0)
                    {
                        anim.SetFloat("Forward", 1.0f, 0.1f, Time.deltaTime);
                    }

                }

                //if (!states.canRun)
                //{
                //    anim.SetFloat("Forward", states.vertical, 0.1f, Time.deltaTime);
                //    anim.SetFloat("Sideways", states.horizontal, 0.1f, Time.deltaTime);

                //}
                //else
                //{
                //    float movement = Mathf.Abs(states.vertical) + Mathf.Abs(states.horizontal);

                //    bool walk = states.walk;

                //    movement = Mathf.Clamp(movement, 0, (walk || states.reloading) ? 0.5f : 1);

                //    anim.SetFloat("Forward", movement, 0.1f, Time.deltaTime);
                //}

                //if (rb.velocity.magnitude > 1)
                //{
                //    anim.SetFloat("Forward", 32.0f, 0.1f, Time.deltaTime);
                //}
                //else
                //{
                //    anim.SetFloat("Forward", 0.0f, 0.1f, Time.deltaTime);

                //}

            }
            yield return new WaitForFixedUpdate ();

        }
    }

    //void UpdateClient2()
    [Client]
    IEnumerator UpdateClient2 () //DUNNO
    {
        while (true) {
            if (!hasAuthority) {
                yield return null;
                continue;
            }

            if (states == null) {
                states = GetComponent<StateManager> ();
            }
            bool onGround = states.onGround;

            if (!onGround && rb.velocity.y < 11) {
                anim.SetBool ("isJumping", false); //change to network animator
            }
            if (!onGround && rb.velocity.y > 1) {
                anim.SetBool ("isJumping", true);
            }

            anim.SetBool ("isGrounded", onGround);

            yield return null;

        }

    }

    void SetupAnimator () {
        anim = GetComponent<Animator> ();

        Animator[] anims = GetComponentsInChildren<Animator> ();

        for (int i = 0; i < anims.Length; i++) {
            if (anims[i] != anim) {
                anim.avatar = anims[i].avatar;
                Destroy (anims[i]);
                break;
            }
        }
    }

    [Client]
    public void StartReload () {
        if (!states.reloading) {
            networkAnimator.SetTrigger ("Reload");
            //anim.SetTrigger("Reload");
        }
    }
}