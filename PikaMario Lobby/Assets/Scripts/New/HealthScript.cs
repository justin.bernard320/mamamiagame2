using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.UI;
public class HealthScript : NetworkBehaviour {

    [SyncVar]
    public int Health;
    Text ammoText;


    Text healthText;
    Image crosshair2;
    Animator anim;

    [SyncVar]
    public string reoccuringName;

    [SerializeField]
    private HandleShooting handleShooting = null;

    [SerializeField]
    private NetworkAnimator networkAnimator = null;

    public GameObject thisGameObject;

    bool IsDead = false;

    StateManager states;

    private void Start()
    {
        Health = 100;

        healthText = GameObject.Find("HealthText").GetComponent<Text>();
        crosshair2 = GameObject.Find("Crosshair2").GetComponent<Image>();
        ammoText = GameObject.Find("AmmoText").GetComponent<Text>();

        //GameOverManager.instance.bHidePanelsBeginning = false; Update in gamePlayers
    }

    public override void OnStartLocalPlayer () {
        Debug.Log ($"OnStartClient HealthScript");
        anim = GetComponent<Animator> ();
        states = GetComponent<StateManager> ();
        reoccuringName = GetComponent<PlayerName> ().playerNameTag;

        healthText = GameObject.Find ("HealthText").GetComponent<Text> ();
        crosshair2 = GameObject.Find ("Crosshair2").GetComponent<Image> ();
        ammoText = GameObject.Find("AmmoText").GetComponent<Text>();

        gameObject.tag = "Player";

        StartCoroutine (UpdateClient ());

        StartCoroutine (DeleteMario ());
    }

    // public override void OnStartClient () {
        //StartCoroutine(UpdateClient());
    // }

    IEnumerator DeleteMario () {
        yield return new WaitForSeconds (8.2f);

        if (!hasAuthority) {
            //Destroy(gameObject);
        }

    }

    [Client]
    IEnumerator UpdateClient () //DUNNO
    {
        while (true) 
        {
            if (netIdentity.hasAuthority) 
            {
                if (!hasAuthority) //????
                {
                    yield return null;
                    continue;
                }

                //if (gameObject.transform.position.y < -22f) // Original (Peach Castle At 0 Y-height)
                if (gameObject.transform.position.y < 128.0f) 
                {
                    Health = 0;
                }

                if (Health <= 0) {
                    Die ();
                }

                //if (IsDead)
                //{
                //    Health = 0;
                //}

                reoccuringName = GetComponent<PlayerName> ().playerNameTag;

                //if (!hasAuthority) { return; }
                if (!hasAuthority) {
                    yield return null;
                    continue;
                }

                healthText.text = "Health: " + Health.ToString ();
                //ammoText.text = "AMMO: " + handleShooting.curBullets.ToString(); //???

                crosshair2.enabled = true;

            }

            yield return null;

        }
    }


    public void TakeDamage (int damage) 
    {
        Health -= damage;
        Debug.Log (Health);
    }

    private void Die () {
        if (anim && !IsDead) {
            IsDead = true;

            //   gameObject.tag = "Dead";

            states.audioManager.PlayDeathSound ();


            networkAnimator.SetTrigger ("Death");
            // anim.SetBool("Death", true);
            GetComponent<PlayerMovement> ().enabled = false;
            GetComponent<PlayerMovement>().bStopMovement = true;

            GetComponent<InputHandler>().bStopAiming = true;

            GetComponent<HandleAnimations> ().enabled = false;
            GetComponent<HandleShooting> ().enabled = false;

            StartCoroutine(Respawn(thisGameObject)); //switch aroo?
            CmdRespawn2 ();
        }
    }

    private void OnDestroy () //put on Pikachus AFTER
    {
        if (IsDead == false) {
            // GameOverManager.instance.MariosAlive--;
        }
    }

    [Command]
    void CmdRespawn2 () 
    {
        StartCoroutine (Respawn (thisGameObject));

    }

    IEnumerator Respawn (GameObject go) {
        //NetworkServer.UnSpawn(this.gameObject);
        //Transform newPos = NetworkManager.singleton.GetStartPosition();
        //go.transform.position = newPos.position;
        //go.transform.rotation = newPos.rotation;

        yield return new WaitForSeconds (3.2f); //3.2

        Health = 100;
        RpcHealth();

        //anim.SetBool("Death", false);
        networkAnimator.SetTrigger ("Revive");
        GetComponent<PlayerMovement> ().enabled = true;
        GetComponent<PlayerMovement>().bStopMovement = false;

        GetComponent<InputHandler>().bStopAiming = false;

        GetComponent<HandleAnimations> ().enabled = true;
        GetComponent<HandleShooting> ().enabled = true;

        Transform newPos = NetworkManager.singleton.GetStartPosition ();
        this.gameObject.transform.position = newPos.position;
        this.gameObject.transform.rotation = newPos.rotation;
        IsDead = false;
        handleShooting.curBullets = handleShooting.BulletsMax; //current bullet script

        NetworkServer.Spawn (thisGameObject, connectionToClient);
        //GetComponent<PlayerName> ().playerNameTag = reoccuringName; //?????????
    }

    [ClientRpc]
    void RpcHealth()
    {
        //yield return new WaitForSeconds(1.2f); //3.2
        Health = 112;

    }

    [Client]
    public void RespawnPikachu (GameObject go) //NEVER GETS CALLED!
    {
        if (!hasAuthority) { return; }

        IsDead = false;
        Health = 100;

        Transform newPos = NetworkManager.singleton.GetStartPosition ();

        GameObject playerSpawn = Instantiate (go, //on server, put over network
            newPos.position,
            newPos.rotation);

        //  NetworkServer.DestroyPlayerForConnection(connectionToClient);
        // NetworkServer.Spawn(playerSpawn, connectionToClient);
        //NetworkServer.AddPlayerForConnection(connectionToClient, playerSpawn);
        NetworkServer.ReplacePlayerForConnection (connectionToClient, playerSpawn, true); //works on server, not client
        NetworkServer.UnSpawn (gameObject);
        Destroy (gameObject);

        playerSpawn.GetComponent<KillCount> ().kills = GetComponent<KillCount> ().kills;
        playerSpawn.GetComponent<PlayerName> ().playerNameTag = reoccuringName; //move down?
    }

    [Client]
    public void RespawnMario (GameObject go) {
        if (!hasAuthority) { return; }

        //anim.SetBool("Death", false);
        networkAnimator.SetTrigger ("Revive");
        GetComponent<PlayerMovement> ().enabled = true;
        GetComponent<HandleAnimations> ().enabled = true;
        GetComponent<HandleShooting> ().enabled = true;

        Transform newPos = NetworkManager.singleton.GetStartPosition ();
        this.gameObject.transform.position = newPos.position;
        this.gameObject.transform.rotation = newPos.rotation;
        IsDead = false;
        Health = 100;
        handleShooting.curBullets = handleShooting.BulletsMax; //current bullet script

        // NetworkServer.Spawn(thisGameObject, connectionToClient);
    }
}