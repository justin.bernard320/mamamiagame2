using System.Collections;
using System.Collections.Generic;
using Mirror;
using TMPro;
using UnityEngine;

public class PlayerName : NetworkBehaviour {
    public TMP_Text displayNameText = null;

    [SyncVar] //KEEP ON
    public string playerNameTag;

    // public TMP_InputField nameInput;
    public Canvas canvasDisplay = null;

    public override void OnStartAuthority () {
        // nameInput = GameObject.Find("Name_InputField").GetComponent<TMP_InputField>();
        // playerNameTag = "Test";

        // StartCoroutine(UpdateClient());
    }

    public override void OnStartLocalPlayer () {
        //StartCoroutine (UpdateClient ());
    }

    void Start () {
         StartCoroutine(UpdateClient());
    }

    IEnumerator UpdateClient () 
    {
        Debug.Log($"Starting UpdateClient on PlayerName");
        while (true) 
        {

            //  int kills = GetComponent<KillCount>().kills;
            if (canvasDisplay != null && Camera.main != null) {
                canvasDisplay.transform.forward = Camera.main.transform.forward;
            }

            if (displayNameText != null) {
                //displayNameText.text = playerNameTag + "K:" + kills;
                displayNameText.text = playerNameTag;
            }

            //if (!hasAuthority) { return; }

            if (!hasAuthority)
            {
                yield return null;
                continue;
            }

            if (displayNameText != null)
            {
                displayNameText.text = "";
            }

            yield return null;
        }

    }

    private void LateUpdate () //TEST!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! YOU ATE WIT DAD
    {
        if (!hasAuthority) { return; }

        if (displayNameText)
        {
            displayNameText.text = "";
        }
    }

    [Server]
    void ServerCheck () {
        {
            //displayNameText.text = playerNameTag;
        }
    }

    [Command]
    void CmdNameChange () {

        //if (displayNameText)
        {
            //displayNameText.text = playerNameTag;
        }

        RpcNameChange ();

    }

    [ClientRpc]
    void RpcNameChange () {
        if (canvasDisplay) {
            //canvasDisplay.transform.forward = Camera.main.transform.forward;
        }

        // playerNameTag = nameInput.text;

        // if (displayNameText)
        {
            //displayNameText.text = playerNameTag;
        }
    }
}