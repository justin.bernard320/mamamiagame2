using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.UI;
public class HealthScriptPikachu : NetworkBehaviour
{
    [SyncVar] //DUNNO?
    public int Health = 30;
    Text healthText;
    Text ammoText;
    Image crosshair2;
    public Animator anim;

    [SyncVar]
    string reoccuringName;

    [SerializeField]
    private NetworkAnimator networkAnimator = null;

    public GameObject SpawnedGameObject;
    private GameObject crosshair;

    public PickupStarPikachu pikachuStar = null;

    //[SyncVar]
    public bool IsDead = false;
    //[SyncVar]
    public bool IsDeadAnimate = false;

    public AudioSource pikachuDie;
    public AudioClip[] pikachuDieClips;

    NetworkSpawnPositions[] spawnPositions;

    void Start () {
        // StartCoroutine (WaitForGameManager ());
        pikachuStar = GetComponent<PickupStarPikachu>();
        //StartCoroutine(UpdateClient());

        StartCoroutine(UpdateClient());

        healthText = GameObject.Find("HealthText").GetComponent<Text>();
        ammoText = GameObject.Find("AmmoText").GetComponent<Text>();
        crosshair2 = GameObject.Find("Crosshair2").GetComponent<Image>();

        //GameOverManager.instance.bHidePanelsBeginning = false; Update in GamePlayers

        spawnPositions = FindObjectsOfType<NetworkSpawnPositions>();

    }

    public override void OnStartAuthority()
    {
        GameOverManager.instance.CursorPrefab.SetActive(false); //Mario in HandleShooting
    }

    public override void OnStartLocalPlayer () 
    {
        Debug.Log ($"OnStartClient HealthScriptPikachu");
        //StartCoroutine (UpdateClient ());
        networkAnimator = GetComponent<NetworkAnimator> (); //check i dunno

        StartCoroutine(UpdateClient());


        //pikachuStar = GetComponent<PickupStarPikachu>();

        healthText = GameObject.Find("HealthText").GetComponent<Text>();
        ammoText = GameObject.Find("AmmoText").GetComponent<Text>();
        crosshair2 = GameObject.Find("Crosshair2").GetComponent<Image>();

        //GameOverManager.instance.PikachusAlive++;

        //StartCoroutine(WaitForGameManager());
        //StartCoroutine(UpdateClient());

        IsDeadAnimate = false;

        GameOverManager.instance.PikachusAlive++;

        gameObject.tag = "Player";
    }

    IEnumerator WaitForGameManager () {
        while (GameOverManager.instance == null) {
            yield return null;
        }
        //GameOverManager.instance.PikachusAlive++;
    }

    //void Update()
    IEnumerator UpdateClient()
    {

        while (true)
        {
            if (netIdentity.hasAuthority)
            {
                //if (gameObject.transform.position.y < -22f) //For Peach Castle = 0 Y-height
                if (gameObject.transform.position.y < 128.0f)
                {
                    Health = 0;
                    Die();
                    IsDead = true;
                    GetComponent<PlayerController_Platform>().enabled = false;

                }

                if (IsDead)
                {
                    //GetComponent<CharacterController>().enabled = false;
                }
                else
                {
                    //GetComponent<CharacterController>().enabled = true;

                }

                //NOT WORKING -------------------------------------------------------
                //if (healthText == null) {
                //    healthText = GameObject.Find ("HealthText").GetComponent<Text> ();
                //}
                //if (ammoText == null) {
                //    ammoText = GameObject.Find ("AmmoText").GetComponent<Text> ();
                //}
                //if (crosshair2 == null) {
                //    crosshair2 = GameObject.Find ("Crosshair2").GetComponent<Image> ();
                //}


                if (Health <= 0)
                {
                    Health = 0;
                }

                //if (IsDead)
                //{
                //    Die();
                //    pikachuStar.bStarMode = false;
                //}


                //if (!hasAuthority) { return; }


                //if (!hasAuthority) {
                //    yield return null;
                //    continue;
                //}



                //if (healthText) 
                {
                    healthText.text = "Health: " + Health.ToString();
                }
                //if (ammoText) 
                {
                    ammoText.text = "";
                }
                //if (crosshair2) 
                {
                    // crosshair2.enabled = false;
                }

                //if (crosshair == null)
                //{
                //    crosshair = GameObject.Find("Crosshair Manager");
                //}

                //if (crosshair.activeSelf == true)
                //{
                //    crosshair.SetActive(false);
                //}

            }
            yield return null;
        }
    }


    

    public void TakeDamage (int damage, GameObject shooter) 
    { //Called on server
        
        //if (pikachuStar.bStarMode) { return; } //yup

        //Health -= damage;
        //Debug.Log ($"Damage taken on {gameObject.name}");
        RpcTakeDamage (damage, shooter);
    }

    [ClientRpc]
    void RpcTakeDamage(int damage, GameObject shooter)
    {
        if (pikachuStar.bStarMode) { return; } //yup

        Health -= damage;
        Debug.Log($"Damage taken on {gameObject.name}");

        if (Health <= 0 && !IsDead)
        {
            IsDead = true;

            shooter.GetComponent<KillCount>().KillPlus();
            //shooter.GetComponent<KillCount>().kills++;
            Die();
        }
    }

    public void Die()
    {
        //if (anim && !IsDead) 
        if (!IsDeadAnimate)
        {

            //IsDead = true;
            IsDeadAnimate = true;

            //gameObject.tag = "Dead";

            GameOverManager.instance.PikachusDead++;
            PikachuDeathSound();

            networkAnimator.SetTrigger("Death");
            GetComponent<PlayerController_Platform>().enabled = false;

            // if (hasAuthority) { return; }

            //   states.audioManager.PlayDeathSound();

            CmdRespawn();
        }
    }

    [Command]
    void CmdRespawn () {
        StartCoroutine (Respawn (SpawnedGameObject));
    }

    [Server]
    IEnumerator Respawn (GameObject go) //Mario
    {

        yield return new WaitForSeconds (1.0f); //1.0!!!!!!!!!!!!!!!!!!!!!


        transform.position = transform.position + new Vector3 (0f, 15f, 0f);

        // yield return new WaitForSeconds(3.20f);

        IsDead = false;
        IsDeadAnimate = false;
        Health = 100;

        Transform newPos = NetworkManager.singleton.GetStartPosition ();
        //Transform newPos = GetRandomStartPosition();

        GameObject playerSpawn = Instantiate(TurnManager.instance.MarioPrefab, //on server, put over network
                newPos.position,
                newPos.rotation);
        //GameObject playerSpawn = Instantiate (go, //on server, put over network
        //    Vector3.zero,
        //    Quaternion.identity); //COMEBACK

        playerSpawn.GetComponent<KillCount> ().kills = GetComponent<KillCount> ().kills;
        playerSpawn.GetComponent<PlayerName> ().playerNameTag = GetComponent<PlayerName> ().playerNameTag; //move down?
        playerSpawn.GetComponent<HealthScript>().reoccuringName = GetComponent<PlayerName>().playerNameTag; //move down?

        StartCoroutine(NameSpawn (playerSpawn));

        //NetworkServer.DestroyPlayerForConnection(connectionToClient);
        //NetworkServer.Spawn(playerSpawn, connectionToClient);
        //NetworkServer.AddPlayerForConnection(connectionToClient, playerSpawn);
        //NetworkServer.ReplacePlayerForConnection(connectionToClient, playerSpawn, true); //works on server, not client
        //Destroy(gameObject);

        // NetworkServer.Destroy(gameObject);
        // NetworkServer.Spawn(playerSpawn, connectionToClient);
        //NetworkServer.ReplacePlayerForConnection(connectionToClient, playerSpawn, true); //works on server, not client

    }

    IEnumerator NameSpawn (GameObject playerSpawn) {
        yield return new WaitForSeconds (0.03f);
        playerSpawn.GetComponent<PlayerName> ().playerNameTag = GetComponent<PlayerName> ().playerNameTag; //move down?

        NetworkServer.ReplacePlayerForConnection (connectionToClient, playerSpawn, true); //works on server, not client
        NetworkServer.Destroy (gameObject);
    }

    //[Client]
    public void RespawnMario (GameObject go) //Mario
    {
        IsDead = false;
        IsDeadAnimate = false;
        Health = 100;

        //Transform newPos = NetworkManager.singleton.GetStartPosition ();
        Transform newPos = GetRandomStartPosition();

        GameObject playerSpawn = Instantiate (go, //on server, put over network
            newPos.position,
            newPos.rotation);

        playerSpawn.GetComponent<KillCount> ().kills = GetComponent<KillCount> ().kills;
        playerSpawn.GetComponent<PlayerName> ().playerNameTag = GetComponent<PlayerName> ().playerNameTag; //move down?

        NetworkServer.DestroyPlayerForConnection (connectionToClient);
        NetworkServer.Spawn (playerSpawn, connectionToClient);
        NetworkServer.AddPlayerForConnection (connectionToClient, playerSpawn);
        NetworkServer.ReplacePlayerForConnection (connectionToClient, playerSpawn, true); //works on server, not client

        Destroy (gameObject);

    }

    Transform GetRandomStartPosition()
    {
        return spawnPositions[Random.Range(0, spawnPositions.Length)].transform;
    }

    [Client]
    public void RespawnPikachu (GameObject go) {
        //anim.SetBool("Death", false);
        networkAnimator.SetTrigger ("Revive");
        GetComponent<PlayerController_Platform> ().enabled = true;

        Transform newPos = NetworkManager.singleton.GetStartPosition ();
        this.gameObject.transform.position = newPos.position;
        this.gameObject.transform.rotation = newPos.rotation;
        IsDead = false;
        IsDeadAnimate = false;
        Health = 100;

        NetworkServer.Spawn (go, connectionToClient);
    }

    public void PikachuDeathSound () //NEEDS COMMAND
    {
        int ran = Random.Range (0, pikachuDieClips.Length);
        pikachuDie.clip = pikachuDieClips[ran];

        pikachuDie.Play ();
    }
}