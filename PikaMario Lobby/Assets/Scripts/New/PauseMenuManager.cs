using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Mirror;

public class PauseMenuManager : NetworkBehaviour
{
    bool bShowupMenu = false;
    GameObject pauseMenu = null;

    public override void OnStartClient()
    {
        pauseMenu = GameObject.Find("PauseMenu");
        StartCoroutine(UpdateClient());
    }

    IEnumerator UpdateClient()
    {
        while (true)
        {
            PauseMenuFunction();

            yield return null;
        }
    }

    void PauseMenuFunction()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            bShowupMenu = !bShowupMenu;
        }

        if (bShowupMenu)
        {
            pauseMenu.SetActive(true);
        }
        if (!bShowupMenu)
        {
            pauseMenu.SetActive(false);
        }

    }
}
