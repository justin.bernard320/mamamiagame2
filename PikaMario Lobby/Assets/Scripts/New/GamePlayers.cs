using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class GamePlayers : NetworkBehaviour
{
    [SyncVar(hook = nameof(AuthorityHandlePartyOwnerStateUpdated))]
    private bool isPartyOwner = false;

    public static event Action<bool> AuthorityOnPartyOwnerStateUpdated;
    public static event Action ClientOnInfoUpdated;


    //public TMP_Text displayNameText = null;
    //public TMP_Text displayKillText = null;
    // public Canvas canvasDisplay = null;

    //[SyncVar]
    //public string playerNameTag;

    // public TMP_InputField nameInput;

    // [SyncVar]
    //public int kills;

    bool bShowupScore = false;
    GameObject killBoard = null;


    [SyncVar(hook = nameof(ClientHandleDisplayNameUpdated))]
    private string displayName;

    public string transferName;



    public override void OnStartClient()
    {
        //StartCoroutine(UpdateClient());

        if (NetworkServer.active) { return; }

        ((NetworkManagerMario2)NetworkManager.singleton).Players.Add(this);

        if (!isClientOnly) { return; }
    }

    public override void OnStartAuthority()
    {
        //TimerManager.instance.ShowTimer = true;
    }

    void Start()
    {
        killBoard = GameObject.Find("KillScoreBoard"); //DONT use UPDATE

        StartCoroutine(UpdateClient());
        //StartCoroutine(ChangeDeleteTags());

    }

    //IEnumerator ChangeDeleteTags()
    //{
    //    yield return new WaitForSeconds(0.32f);

    //    gameObject.tag = "DeletePlayer";
    //}
    IEnumerator UpdateClient()
    {
        while (true)
        {
            // if (netIdentity.hasAuthority)
            {
                if (!hasAuthority)
                {
                    yield return null;
                    continue;
                }

                //int pikachusNum = FindObjectsOfType<HealthScriptPikachu>().Length; //INSTEAD OF HealthScriptPikachu
                //Text pikachusLeftText = GameObject.Find("PikachusLeftText").GetComponent<Text>();
                //pikachusLeftText.text = "Pikachus Left: " + pikachusNum.ToString();
                GameOverManager.instance.bShowPikaText = true;

                GameOverManager.instance.bHidePanelsBeginning = false; //?
                TimerManager.instance.ShowTimer = true;

                PickupSpawningManager.instance.bStopSpawning = false;
                GetComponent<HandleShooting>().bShowAmmoText = true;


                //if (Input.GetKeyDown(KeyCode.H))
                //{
                //    bShowupScore = !bShowupScore;
                //}

                //if (bShowupScore)
                //{
                //    killBoard.SetActive(true);
                //    Debug.Log("ON");
                //}
                //if (!bShowupScore)
                //{
                //    killBoard.SetActive(false);
                //    Debug.Log("OFF");
                //}

            }
            yield return null;

        }

    }

    //[Command]
    //void CmdNameChange()
    //{
    //  //  playerNameTag = nameInput.text;

    //    if (displayNameText)
    //    {
    //       displayNameText.text = playerNameTag;
    //    }

    //    RpcNameChange();


    //}
    //[ClientRpc]
    //void RpcNameChange()
    //{
    //   // playerNameTag = nameInput.text;

    //    if (displayNameText)
    //    {
    //        displayNameText.text = playerNameTag;
    //    }
    //}

    public string GetDisplayName()
    {
        return displayName;
    }
    public bool GetIsPartyOwner()
    {
        return isPartyOwner;
    }

    //public override void OnStartClient()
    //{
    //    if (NetworkServer.active) { return; }

    //    ((NetworkManagerMario2)NetworkManager.singleton).Players.Add(this);

    //    if (!isClientOnly) { return; }
    //}

    public override void OnStopClient()
    {
        ClientOnInfoUpdated?.Invoke();

        if (!isClientOnly) { return; }

        ((NetworkManagerMario2)NetworkManager.singleton).Players.Remove(this); //CHANGE BACK
    }

    [Server]
    public void SetDisplayName(string displayName)
    {
        this.displayName = displayName;
    }

    [Server]
    public void SetPartyOwner(bool state)
    {
        isPartyOwner = state;
    }

    [Command]
    public void CmdStartGame()
    {
        if (!isPartyOwner) { return; }

        ((NetworkManagerMario)NetworkManager.singleton).StartGame();
    }

    private void AuthorityHandlePartyOwnerStateUpdated(bool oldState, bool newState)
    {
        if (!hasAuthority) { return; }

        AuthorityOnPartyOwnerStateUpdated?.Invoke(newState);
    }

    private void ClientHandleDisplayNameUpdated(string oldDisplayName, string newDisplayName)
    {
        ClientOnInfoUpdated?.Invoke();
    }
}
