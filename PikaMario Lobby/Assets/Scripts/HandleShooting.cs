﻿using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.UI;

public class HandleShooting : NetworkBehaviour {
    StateManager states;
    public Animator weaponAnim;
    public float fireRate;
    float timer;
    public Transform bulletSpawnPoint;
    public GameObject smokeParticle;
    public ParticleSystem[] muzzle;
    public GameObject casingPrefab;
    public Transform caseSpawn;

    //NEW CODE
    public Camera thirdPersonCam;
    //NEW CODE (PUT IN HUD SCRIPT EVENTUALLY!)
    public Text ammoText;
    //NEW CODE
    public LayerMask layerMask = new LayerMask ();

    public int BulletsMax = 30;
    [SyncVar]
    public int curBullets;

    bool bKill = false;

    KillCount killCount = null;

    public bool bShowAmmoText = true; //CHEK HERE

    // [SyncVar]
    public Transform aimHelper;
    private Collider ownCollider;

    /*public override void OnStartAuthority()*/

    //public override void OnStartLocalPlayer () {
    void Start()
    { 
        Debug.Log ($"OnStartClient HandleShooting");
        curBullets = BulletsMax;
        ownCollider = GetComponent<Collider> ();
        states = GetComponent<StateManager> ();
        killCount = GetComponent<KillCount>();

        ammoText = GameObject.Find ("AmmoText").GetComponent<Text> ();
        ammoText.text = "AMMO: " + curBullets.ToString();

        bKill = false;

        StartCoroutine(UpdateClient ());
    }

    bool shoot;
    bool dontShoot;

    bool emptyGun;

    public override void OnStartLocalPlayer() 
    {
        GameOverManager.instance.CursorPrefab.SetActive(true); //Pikachu in HealthScriptPikachu

        ammoText = GameObject.Find("AmmoText").GetComponent<Text>();
        ammoText.text = "AMMO: " + curBullets.ToString();
    }

    //void FixedUpdate()
     [Client]
    IEnumerator UpdateClient() //DUNNO
    {
        while (true) 
        {
          
            if (netIdentity.hasAuthority) 
            {
                
                ammoText.text = "AMMO: " + curBullets.ToString();


                //if (!hasAuthority) { return; } //TODO ////////////////////////////////////////////////

                if (!hasAuthority || GameOverManager.instance.bPikachuWins)
                {
                    //yield return null;
                    yield return new WaitForFixedUpdate();
                    continue;
                }

                //ammoText.text = "AMMO: " + curBullets.ToString();
                //ammoText.text = "AMMO: " + "320";


                //if (bShowAmmoText)
                //{
                //    ammoText.text = "AMMO: " + curBullets.ToString();
                //}
                //else
                //{
                //    ammoText.text = "";
                //}

                //ammoText.text = "AMMO: " + curBullets.ToString();

                shoot = states.shoot;

                if (shoot) {
                    if (timer <= 0) {
                        weaponAnim.SetBool ("Shoot", false);

                        if (curBullets > 0) {
                            emptyGun = false;
                            // states.audioManager.PlayGunSound();

                            GameObject go = Instantiate (casingPrefab, caseSpawn.position, caseSpawn.rotation) as GameObject;
                            Rigidbody rig = go.GetComponent<Rigidbody> ();
                            rig.AddForce (transform.right.normalized * 2 + Vector3.up * 1.3f, ForceMode.Impulse);
                            rig.AddRelativeTorque (go.transform.right * 1.5f, ForceMode.Impulse);

                            for (int i = 0; i < muzzle.Length; i++) {
                                muzzle[i].Emit (1);
                            }

                            RaycastShoot ();
                            states.audioManager.CmdPlayGunSound ();

                            curBullets = curBullets - 1;

                        } else {
                            if (emptyGun) {
                                states.handleAnim.StartReload ();
                                //states.audioManager.PlayGunReload();
                                states.audioManager.CmdPlayGunReload ();
                                curBullets = BulletsMax;
                            } else {
                                states.audioManager.PlayEffect ("empty_gun");
                                emptyGun = true;
                            }
                        }

                        timer = fireRate;
                    } else {
                        weaponAnim.SetBool ("Shoot", true);

                        timer -= Time.deltaTime;
                    }
                } else {
                    timer = -1;
                    weaponAnim.SetBool ("Shoot", false);
                }

            }


            yield return new WaitForFixedUpdate ();
        }
    }

    void RaycastShoot () 
    {
        Vector3 posOrigin = thirdPersonCam.transform.position;
        if (Physics.Raycast (posOrigin, thirdPersonCam.transform.forward, out RaycastHit hit, 3200, layerMask)) 
        {
            Debug.Log ($"CmdRaycastShoot {posOrigin} > {hit.point}");
            Debug.DrawRay (posOrigin, thirdPersonCam.transform.forward * 3000, Color.red, 2.0f);

            CmdRaycastShoot(hit.transform.gameObject, hit.point);

        }
    }

    [Command]
    void CmdRaycastShoot (GameObject hitTarget, Vector3 hitPoint)
    {
        //RpcRaycastShoot(hitTarget, hitPoint);

        if (hitTarget != null) 
        {

            GameObject go = Instantiate (smokeParticle, hitPoint, Quaternion.identity) as GameObject;
            go.transform.LookAt (bulletSpawnPoint.position);

            NetworkServer.Spawn (go, connectionToClient);

            if (hitTarget.GetComponent<HealthScript> ()) //Mario
            {
                hitTarget.GetComponent<HealthScript> ().TakeDamage (10);
                Debug.Log ($"CmdRaycastShoot - Hit Mario");
            }

            if (hitTarget.GetComponent<HealthScriptPikachu> ()) //Pikachu
            {
                hitTarget.GetComponent<HealthScriptPikachu> ().TakeDamage (10, gameObject);
                Debug.Log ($"CmdRaycastShoot - Hit Pikachu");
                //killCount.kills++;
            }

        }
        else //Hits no target
        {
            GameObject go = Instantiate(smokeParticle, hitPoint, Quaternion.identity) as GameObject;
            go.transform.LookAt(bulletSpawnPoint.position);

            NetworkServer.Spawn(go, connectionToClient);
        }
    }


    void KillCount2()
    {
        killCount.kills++;
        GetComponent<KillCount>().kills++;
    }

    [ClientRpc]
    void RpcKillCount()
    {
        killCount.kills++;
        GetComponent<KillCount>().kills++;
    }

    //[ClientRpc]
    //void RpcRaycastShoot()
    //{
    //    states.audioManager.PlayGunSound();

    //    return;

    //    RaycastHit hit;

    //    Vector3 posOrigin = thirdPersonCam.transform.position;

    //    if (Physics.Raycast(posOrigin, thirdPersonCam.transform.forward, out hit, 3200, layerMask))
    //    {
    //        Debug.DrawRay(posOrigin, thirdPersonCam.transform.forward * 3000, Color.red, 2.0f);

    //        GameObject go = Instantiate(smokeParticle, hit.point, Quaternion.identity) as GameObject;
    //        go.transform.LookAt(bulletSpawnPoint.position);

    //        NetworkServer.Spawn(go, connectionToClient);

    //        if (hit.transform.GetComponent<HealthScript>()) //Mario
    //        {
    //            hit.transform.GetComponent<HealthScript>().TakeDamage(10);
    //        }

    //        if (hit.transform.GetComponent<HealthScriptPikachu>()) //Pikachu
    //        {
    //            hit.transform.GetComponent<HealthScriptPikachu>().TakeDamage(10);
    //        }
    //    }
    //}
}