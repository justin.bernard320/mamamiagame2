using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : NetworkBehaviour
{
    //[SyncVar]
    public GameObject camObject;
    private Camera myCamera;

    private void Awake()
    {
       camObject.SetActive(false);
    }

    public override void OnStartAuthority()
    {
        camObject.SetActive(true);
    }

    public override void OnStartLocalPlayer()
    {
        //camObject.SetActive(true);
    }
}
