using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.SceneManagement;

public class TurnManager : NetworkBehaviour
{
    public static TurnManager instance;

    int numPlayers2;
    public GameObject MarioPrefab;
    public GameObject PikachuPrefab;

    
    public static List<Player> lobbyPlayers = new List<Player>();

    void Awake()
    {
        instance = this;

    }

    public void Start()
    {
        //instance = this;
        StartCoroutine(Spawn());

        //GameOverManager.instance.PikachusAlive = 0;
        //GameOverManager.instance.PikachusDead = 0;
        numPlayers2 = 0;
    }

    public void AddPlayer(Player _player)
    {
        lobbyPlayers.Add(_player);
        NetworkServer.Spawn(_player.gameObject, _player.connectionToClient);
    }

    IEnumerator Spawn()
    {
        yield return new WaitForSeconds(0.32f);

        //GameOverManager.instance.PikachusAlive = 0;
        //GameOverManager.instance.PikachusDead = 0;
        //numPlayers2 = 0;

        int MarioPlayerStart = UnityEngine.Random.Range(1, lobbyPlayers.Count + 1); //gotta add 1 for max on Ints
        Debug.Log("Players: " + lobbyPlayers.Count);
        Debug.Log("Mario Player: " + MarioPlayerStart);

        //if (SceneManager.GetActiveScene().name.StartsWith("CastleScene"))
        {
            foreach (Player player in lobbyPlayers)
            {
                numPlayers2++;



                if (numPlayers2 == MarioPlayerStart)
                {
                    GameObject marioSpawn = Instantiate(MarioPrefab, //on server, put over network
                    NetworkManager.singleton.GetStartPosition().position, Quaternion.identity);

                    StartCoroutine(DisplayNameChange(player, marioSpawn));
                    NetworkServer.Spawn(marioSpawn, player.connectionToClient);
                }
                else
                {
                    GameObject pikachuSpawn = Instantiate(PikachuPrefab, //on server, put over network
                    NetworkManager.singleton.GetStartPosition().position, Quaternion.identity);

                    StartCoroutine(DisplayNameChange(player, pikachuSpawn));
                    NetworkServer.Spawn(pikachuSpawn, player.connectionToClient);
                }
            }
        }
    }

    IEnumerator DisplayNameChange(Player player, GameObject playerSpawn)
    {
        yield return new WaitForSeconds(0.32f);

        playerSpawn.GetComponent<PlayerName>().playerNameTag = player.displayName;

        //  Player player2 = NetworkClient.connection.identity.GetComponent<Player>();
        //player.playerName = ((NetworkManagerMario2)NetworkManager.singleton).OnCreatePlayer(player.connectionToClient);
        //playerSpawn.GetComponent<PlayerName>().playerNameTag = player.playerName;

    }
}
