using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using Mirror;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Match {
    public string matchID;

    public bool publicMatch;
    public bool inMatch;
    public bool matchFull;

    public SyncListGameObject players = new SyncListGameObject ();
    public Match (string matchID, GameObject player) {
        this.matchID = matchID;
        players.Add (player);
    }

    public Match () { }

}

[System.Serializable]
public struct Characters {
    public GameObject characterPrefab;
}

[System.Serializable]
public class SyncListGameObject : SyncList<GameObject> { }

[System.Serializable]
public class SyncListMatch : SyncList<Match> { }

public class MatchMaker : NetworkBehaviour {

    public static MatchMaker instance;

    public SyncListMatch matches = new SyncListMatch ();
    //public SyncListString matchIDs = new SyncListString();
    public SyncList<string> matchIDs = new SyncList<string> ();

    [SerializeField] GameObject turnManagerPrefab;
    [SerializeField] GameObject gameOverManagerPrefab;

    [SerializeField] GameObject MarioPrefab;
    [SerializeField] GameObject PikachuPrefab;

    GameObject gameOverManager;


    [SerializeField] List<Characters> characterPrefabs = new List<Characters> ();

    List<Player> matchPlayers; //NEW

    public string PlayerName { get; set; }
    public InputField nameInputObject;

    private void Start () {
        instance = this;
    }

    public bool HostGame (string _matchID, GameObject _player, bool publicMatch, out int playerIndex) {
        playerIndex = -1;

        if (!matchIDs.Contains (_matchID)) {
            matchIDs.Add (_matchID);
            Match match = new Match (_matchID, _player);
            match.publicMatch = publicMatch;
            matches.Add (match);
            Debug.Log ($"Match ID generated");
            playerIndex = 1;
            return true;
        } else {
            Debug.Log ($"Match ID already exists");
            return false;
        }
    }

    public bool JoinGame (string _matchID, GameObject _player, out int playerIndex) {
        playerIndex = -1;

        if (matchIDs.Contains (_matchID)) {
            for (int i = 0; i < matches.Count; i++) {
                if (matches[i].matchID == _matchID) {
                    matches[i].players.Add (_player);
                    playerIndex = matches[i].players.Count;
                    break;
                }
            }

            Debug.Log ($"Match joined");
            return true;
        } else {
            Debug.Log ($"Match ID does not exist");
            return false;
        }
    }

    public bool SearchGame (GameObject _player, out int playerIndex, out string matchID) {
        playerIndex = -1;
        matchID = string.Empty;

        for (int i = 0; i < matches.Count; i++) {
            if (matches[i].publicMatch && !matches[i].matchFull && !matches[i].inMatch) {
                matchID = matches[i].matchID;
                if (JoinGame (matchID, _player, out playerIndex)) {
                    return true;
                }
            }
        }

        return false;
    }

    public void BeginGame(string _matchID)
    {
        //if (gameOverManager != null)
        //{
        //    NetworkServer.Destroy(gameOverManager);
        //}


        //if (GameOverManager.instance != null)
        if (gameOverManager != null)
        {
            gameOverManager.GetComponent<GameOverManager>().ChaseLevelServer();
            //GameOverManager.instance.ChaseLevelServer();
        }
        else
        {
            //Do spawning stuff
            gameOverManager = Instantiate(gameOverManagerPrefab);
            NetworkServer.Spawn(gameOverManager);
        }


        GameObject newTurnManager = Instantiate(turnManagerPrefab);

        NetworkServer.Spawn (newTurnManager);

        //gameOverManager.GetComponent<GameOverManager>().bHidePikachuCount = true; //works
        //gameOverManager.GetComponent<GameOverManager>().ChaseLevelServer();

        UILobby.instance.bHideCanvas = false;

        //gameOverManager.GetComponent<TurnOffGameOverPanels>().bTurnOnGameOverManager = true; //turn ON HUD on lobby
        // GameObject.Find("TurnOnGameWin").SetActive(true);
        //GameObject.Find("GameOverCanvas").SetActive(true);


        TimerManager.instance.timeValue = 200; //important


        newTurnManager.GetComponent<NetworkMatchChecker> ().matchId = _matchID.ToGuid ();
        TurnManager turnManager = newTurnManager.GetComponent<TurnManager> ();

        for (int i = 0; i < matches.Count; i++)
        {
            if (matches[i].matchID == _matchID) 
            {
                // List<Player> matchPlayers = new List<Player> ();
                matchPlayers = new List<Player>();

                Debug.Log ($"BeginGame {_matchID} | {matches[i].players.Count} Players.");

                foreach (var player in matches[i].players) {
                    Player _player = player.GetComponent<Player> ();
                     matchPlayers.Add (_player);
                     //turnManager.AddPlayer (_player); ///////
                    _player.StartGame ();
                }
                Debug.Log ($"Game Started. Spawning {matchPlayers.Count} Players.");
                SpawnPlayers (matchPlayers, _matchID);
                break;
            }
        }
    }

    public void SpawnPlayers (List<Player> matchPlayers2222, string _matchID) {

        List<Characters> charactersAvailable = new List<Characters> ();

        foreach (Characters character in characterPrefabs) {
            charactersAvailable.Add (character);
        }

        foreach (Player player in matchPlayers) {
            int index = UnityEngine.Random.Range (0, charactersAvailable.Count);
            Characters selectedCharacter = charactersAvailable[index];
            Debug.Log ($"Spawning character {selectedCharacter.characterPrefab.name} for Player {player.playerIndex}");

            GameObject characterSpawn = Instantiate(selectedCharacter.characterPrefab, new Vector3(0.0f, 160.3f, 0.0f), Quaternion.identity); //SPAWN HEIGHT
            //GameObject characterSpawn = Instantiate (selectedCharacter.characterPrefab, Vector3.zero, Quaternion.identity);
            //GameObject characterSpawn = Instantiate(MarioPrefab, Vector3.zero, Quaternion.identity);

            if (characterSpawn.TryGetComponent<NetworkMatchChecker> (out NetworkMatchChecker networkMatchChecker)) 
            {
                networkMatchChecker.matchId = _matchID.ToGuid ();
            }

            StartCoroutine (DisplayNameChange (player, characterSpawn));
            NetworkServer.Spawn (characterSpawn, player.connectionToClient);
            //charactersAvailable.RemoveAt (index);
        }
    }

    IEnumerator DisplayNameChange(Player player, GameObject playerSpawn)
    {
        yield return new WaitForSeconds(0.32f);

        playerSpawn.GetComponent<PlayerName>().playerNameTag = player.displayName;
    }


    public static string GetRandomMatchID () {
        string _id = string.Empty;
        for (int i = 0; i < 5; i++) {
            int random = UnityEngine.Random.Range (0, 36);
            if (random < 26) {
                _id += (char) (random + 65);
            } else {
                _id += (random - 26).ToString ();
            }
        }
        Debug.Log ($"Random Match ID: {_id}");
        return _id;
    }

    public void PlayerDisconnected (Player player, string _matchID) {
        for (int i = 0; i < matches.Count; i++) {
            if (matches[i].matchID == _matchID) {
                int playerIndex = matches[i].players.IndexOf (player.gameObject);
                matches[i].players.RemoveAt (playerIndex);
                Debug.Log ($"Player disconnected from match {_matchID} | {matches[i].players.Count} players remaining");

                if (matches[i].players.Count == 0) {
                    Debug.Log ($"No more players in Match. Terminating {_matchID}");
                    matches.RemoveAt (i);
                    matchIDs.Remove (_matchID);
                }
                break;
            }
        }
    }

}

public static class MatchExtensions {
    public static Guid ToGuid (this string id) {
        MD5CryptoServiceProvider provider = new MD5CryptoServiceProvider ();
        byte[] inputBytes = Encoding.Default.GetBytes (id);
        byte[] hashBytes = provider.ComputeHash (inputBytes);

        return new Guid (hashBytes);
    }
}